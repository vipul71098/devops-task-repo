FROM node:alpine AS development
WORKDIR /
COPY ./package.json /
RUN npm install
COPY . .
EXPOSE 3000
CMD npm start
