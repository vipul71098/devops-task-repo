import React, { useEffect, useState } from "react";
import "./App.css";
import logo from "./test.jpeg";
function App() {
  const [time, setTime] = useState(null);

  setInterval(() => {
    setTime(new Date().toTimeString());
  }, 1000);

  return (
    <div className="App">
       <h1>This is Demo React App For Devops Buddies..........!</h1>
      <h2>{time ? time : null}</h2>
      <img src={logo}  />
    </div>
  );
}

export default App;
